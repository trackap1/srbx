require("dotenv").config();
import {Travis} from "./lib";
import {setTimeout} from 'timers/promises'

const tokens = process.env.TRAVIS_TOKENS!.split(",");

(async () => {
    for (const acc of tokens){
        const api = new Travis(acc)
        console.log("Cancel Builds")
        await api.cancelBuilds()
        await setTimeout(30000)
        console.log("Build Starting")
        await api.consumeOSSCreditsEnable()
        await api.runBuilds()
        await setTimeout(30000)
        await api.consumeOSSCreditsDisable()
    }
})().then(() => process.exit(0)).catch((err: any) =>{ console.error(err); process.exit(1) })